Serial Listener is a library for arduino uno board to upload a firmware able to send commands and drive the board via serial communication .

# Arduino Firmware

SerialListener_firmware.ino in arduino_library/SerialListener/examples/SerialListener_firmware

  usage : 
  upload this firmware to your arduino uno board
  then open the serial monitor and send upercase command to your board : 
  
  - PINMODE Dx INPUT|OUTPUT|PULLUP adjust Dx pin mode
  - READ ID return "SERIAL LISTENER version" to serial
  - READ Ax|Dx read analogic|digital pin and return "V Ax|Dx value" to serial
  - WRITE Dx HIGH|LOW|x change pin's state, x is only available on PWM pins
  - LISTEN Ax|Dx listen pin state and send value when changing (more than ANALOGDELTA on anolog pins, default value is 2)
  - CLEAR Ax|Dx stop listening Ax|Dx pin 

