/*
  serialcommand.cpp - Library for serial commands in an arduino firmware
  author : Thierry Dassé
  date : 2020-03-02
  version : 1.0
  license : cc-by-nc-sa 
*/

#include "SerialCommand.h"

void SerialCommand::init(long speed) {
	
	Serial.begin(speed);
	this->resetCommand();
}

void SerialCommand::readSerial() {
	
	if (Serial.available()) {
		char c = Serial.read();
		this->buffer[this->pos++] = c;
    }
}

bool SerialCommand::endOfCommand() {

	if ((this->pos >0) & (this->buffer[this->pos-1] == '\n')) {
		this->buffer[this->pos-1] = '\0';
		return true;
	}
	
	return false;
}

bool SerialCommand::containsAt(char *com) {
 
	int i = 0;
	while (com[i] !='\0') {
		if ((this->current)[i] != com[i]) {
			return false;
		}
		i++;
	}
	if (((this->current)[i] !=' ') and ((this->current)[i] !='\0')) {
		return false;
	}
	
	while ((this->current)[i] ==' ') {
		i++;
	}
	
	this->current += i;
	return true; 	
}

bool SerialCommand::gotoNext() {
	
	int i = 0;
	
	while (((this->current)[i] !=' ') and ((this->current)[i] !='\0')) {
		i++;
	}
	
	while ((this->current)[i] ==' ') {
		i++;
	}
	
	if ((this->current)[i] =='\0') {
		return false;
	}
	
	this->current += i;
	return true;
}


char *SerialCommand::getAt() {
	return this->current;
}

char *SerialCommand::getCommand() {
	return this->buffer;
}

void SerialCommand::resetCommand() {
	
	this->current = this->buffer;
	this->pos = 0;
}

