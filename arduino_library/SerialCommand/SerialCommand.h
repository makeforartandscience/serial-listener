/*
  serialcommand.h - Library for serial commands in an arduino firmware
  author : Thierry Dassé
  date : 2020-03-02
  version : 1.0
  license : cc-by-nc-sa 
*/

#ifndef serialcommand_h
#define serialcommand_h

#include "Arduino.h"

#define SC_BUFFER_SIZE 64

class SerialCommand {
  public:
  void init(long speed);
  void readSerial();
  bool endOfCommand();
  bool containsAt(char *com);
  bool gotoNext();
  char *getAt();
  char *getCommand();
  void resetCommand();
 
  private:
  char buffer[SC_BUFFER_SIZE];
  int pos;
  char *current;
};

#endif
