/*
  seriallistener.h - Library for serial commands in an arduino uno firmware
  author : Thierry Dassé
  date : 2020-03-04
  version : 1.0
  license : cc-by-nc-sa 
*/

#ifndef seriallistener_h
#define seriallistener_h

#include "Arduino.h"
#include "SerialCommand.h"

//values for arduino uno
#define DIGITAL 0
#define ANALOG 1
#define DIGITALPIN 14
#define ANALOGPIN  6
#define ANALOGDELTA 2
#define ID "SERIAL LISTENER 1.0"

class SerialListener {
  public:
  void init(long speed);
  void readSerial();
  
  private:
  void initPin();
  bool digitalListen[DIGITALPIN];
  char digitalPrevious[DIGITALPIN];
  bool analogListen[ANALOGPIN];
  int analogPrevious[ANALOGPIN];
  SerialCommand sc;
};

#endif
