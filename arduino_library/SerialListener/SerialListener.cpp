/*
  seriallistener.h - Library for serial commands in an arduino uno firmware
  author : Thierry Dassé
  date : 2020-03-04
  version : 1.0
  license : cc-by-nc-sa 
*/

#include "SerialListener.h"

char sl_pwm[] = {6,3,5,6,9,10,11}; //pwm pin, first number is array size

bool sl_isPwm(int pin) {
	
	int i = 1;
	while (i <= sl_pwm[0]) {
		if (pin == sl_pwm[i]) {
			return true;
		}
		i++;
	}
	return false;
}

int strtoint(char *c) {
	
	int pos = 0;
	int val = 0;
	while ((c[pos] != '\0') and (c[pos] != ' ')) {
		val = 10*val + c[pos++] - '0';
	}
	return val;
}

void SerialListener::init(long speed) {
	
	this->sc.init(speed);
	this->initPin();
}

void SerialListener::initPin() {
  
	for(int i = 0; i<DIGITALPIN;i++) {
		digitalListen[i] = 0;
	}
	for(int i = 0; i<ANALOGPIN;i++) {
		analogListen[i] = 0;
	}
}
 
void SerialListener::readSerial() {
	
	bool error = false;
	char *params = NULL;
  
	this->sc.readSerial();

	if (this->sc.endOfCommand()) {
		if (this->sc.containsAt("READ")) {
			if (this->sc.containsAt("ID")) {
				//READ ID  
				Serial.println(ID);
			} else {
				params = this->sc.getAt();
				if (params[0] == 'D'){
					//READ Dx
					int pin = strtoint(++params);
					if (pin < DIGITALPIN) {
						Serial.print("V D");
						Serial.print(pin);
						Serial.print(" ");
						Serial.println(digitalRead(pin));
					} else {
						error = true;
					}
				} else if (params[0] =='A') {
					//READ Ax
					int pin = strtoint(++params);
					if (pin < ANALOGPIN) {
						Serial.print("V A");
						Serial.print(pin);
						Serial.print(" ");
						Serial.println(analogRead(pin));
					} else {
						error = true;
					}
				} else {		  
					error = true;
				}
			}

		} else if (this->sc.containsAt("PINMODE")) {
			//PINMODE Dx INPUT|OUPUT|PULLUP
			params = this->sc.getAt();
			if (params[0] == 'D'){
				int pin = strtoint(++params);
				if (pin < DIGITALPIN) {
					this->sc.gotoNext();
					if (this->sc.containsAt("INPUT")) {
						pinMode(pin,INPUT);
					} else if (this->sc.containsAt("OUTPUT")) {
						pinMode(pin,OUTPUT);
					} else if (this->sc.containsAt("PULLUP")) {
						pinMode(pin,INPUT_PULLUP);
					} else {
						error = true;
					}
				} else {
					error = true;
				}
			} else {
					error = true;
			}
			
		} else if (this->sc.containsAt("WRITE")) {
			params = this->sc.getAt();
			if (params[0] == 'D'){
				//WRITE Dx LOW|HIGH
				int pin = strtoint(++params);
				if (pin < DIGITALPIN) {
					this->sc.gotoNext();
					if (this->sc.containsAt("LOW")) {
						digitalWrite(pin,LOW);
					} else if (this->sc.containsAt("HIGH")) {
						digitalWrite(pin,HIGH);
					} else if (sl_isPwm(pin)) {
						int value = strtoint(this->sc.getAt());
						if ((value >=0) & (value <= 255)) {
							analogWrite(pin,value);
						} else {
							error = true;
						}
					} else {
						error = true;
					}
				} else {
					error = true;
				}
			} else {
					error = true;
			}
		
		} else if (this->sc.containsAt("LISTEN")) {
			params = this->sc.getAt();
			if (params[0] == 'D'){
				//LISTEN Dx
				int pin = strtoint(++params);
				if (pin < DIGITALPIN) {
					digitalListen[pin] = true;
					digitalPrevious[pin] = 1 - digitalRead(pin);
				} else {
					error = true;
				}
			} else if (params[0] == 'A'){
				//LISTEN Ax
				int pin = strtoint(++params);
				if (pin < ANALOGPIN) {
					analogListen[pin] = true;
					analogPrevious[pin] = -1 - ANALOGDELTA;
 				} else {
					error = true;
				}
			} else {
					error = true;
			}

		} else if (this->sc.containsAt("CLEAR")) {
			params = this->sc.getAt();
			if (params[0] == 'D'){
				//CLEAR Dx
				int pin = strtoint(++params);
				if (pin < DIGITALPIN) {
					digitalListen[pin] = false;
				} else {
					error = true;
				}
			} else if (params[0] == 'A'){
				//CLEAR Ax
				int pin = strtoint(++params);
				if (pin < ANALOGPIN) {
					analogListen[pin] = false;
				} else {
					error = true;
				}
			} else {
					error = true;
			}

		} else {
			error =true;
		}

		if (error) {
			Serial.print("error ");
			Serial.println(this->sc.getCommand());
		}
		
		sc.resetCommand();
	}
	
	for(int i = 0; i<DIGITALPIN;i++) {
		if (digitalListen[i]) {
			int val = digitalRead(i);
			if (val != digitalPrevious[i]) {
				digitalPrevious[i] = val;
				Serial.print("V D");
				Serial.print(i);
				Serial.print(" ");
				Serial.println(val);
			}
		}
	}
	
    for(int i = 0; i<ANALOGPIN;i++) {
		if (analogListen[i]) {
			int val = analogRead(i);
			if (abs(analogPrevious[i]-val) > ANALOGDELTA) {
				analogPrevious[i] = val;
				Serial.print("V A");
				Serial.print(i);
				Serial.print(" ");
				Serial.println(val);
 
			}
		}
	}	
}
