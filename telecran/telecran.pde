import processing.serial.*;
import java.util.Date;
import java.text.SimpleDateFormat;  

Serial port;
PImage intro,fond; 
boolean init = true;
String command ="";
int xMargin,yMargin,screenWidth = 700,screenHeight = 500;
int x1, x2, y1, y2;
String broche;
int value;

SimpleDateFormat sdf;

void setup() {
  size(1024,768);
  intro = loadImage("intro.png");
  fond = loadImage("telecran.png");
  background(0);
  xMargin = (width-1024)/2 + 160;
  yMargin = 132;
  String portName = Serial.list()[0];
  port = new Serial(this,portName,57600);
  strokeWeight(2);
  stroke(96, 96, 96);
  image(intro,(width-1024)/2,0);
      
  sdf = new SimpleDateFormat("yyyyMMddHHmmss");
  }
 
void draw() {
  if (init) {
    delay(2000);
    port.write("PINMODE D2 PULLUP\n"); 
    port.write("PINMODE D3 PULLUP\n"); 
    port.write("LISTEN A0\n"); 
    port.write("LISTEN A1\n"); 
    port.write("LISTEN D2\n");
    port.write("LISTEN D3\n"); 
    image(fond,(width-1024)/2,0);
    init = false;
  }

  while (port.available() > 0) {
    int c = port.read();
    if (c == '\n') {
      if (command.split(" ")[0].equals("V")) {
        broche = command.split(" ")[1];
        value = Integer.parseInt(command.split(" ")[2].trim());
        switch(broche) {
          case "A0":
            x2=(int) map(value,0,1023,0,screenWidth);
            break;
          case "A1":
            y2=(int) map(value,0,1023,0,screenHeight);
            break;
          case "D3":
            image(fond,(width-1024)/2,0);
            break;
          case "D2":
            if (value == 0) {
              Date date = new Date();
              save("T"+sdf.format(date)+".png");
            }
            break;
        }
      } else {
       // println(command);
      }
//      println (x1,",", y1);
      if ((x1 != -1) && (y1 != -1)) {
        line(xMargin + x1,yMargin + y1,xMargin + x2,yMargin + y2);
      }
      x1=x2;
      y1=y2;
      command = "";
    } else {
      command = command + (char)c;
    }
  }
}
    
